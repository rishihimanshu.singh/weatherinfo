import { useLinkProps } from '@react-navigation/native';
import React, { useState } from 'react';
import { View, Text,TextInput, StyleSheet, TouchableOpacity,BackHandler} from 'react-native'
import styles from './Styles';

export default function Screen2(props){

    //console.log("props22` data--", props);
    const [fname, setFname] = useState('');
    const [lname, setLname] = useState('');
    const [address, setAddress] = useState('');
    const [fNameErrMsg,setfNameErrMsg] = useState('')
    const [addrEMsg,setAddrErrMsg] = useState('')

    const nameValidate = (key,frName) =>{
       frName = frName.replaceAll(/\s/g,'')
       frName =frName.replace(/[^a-zA-Z \s ]/g, "")
       key=== "fname"?setFname(frName):setLname(frName)
       //console.log("wwwwww----", frName);
        let reg = /^[A-Za-z .]{2,50}/
        if(reg.test(frName) && key === "fname"  ){

            setfNameErrMsg('')
        }
    }

    // const addressValidate = (addr) =>{
    //    // addr = addr.trim()
    //     setAddress(addr)
    //     // if(addr.length >10){
    //     //     setAddrErrMsg('')
    //     // }

    // }
    const formValidate= (nxt) =>{
        console.log("addr lent", address?.length);
        let addr = address.trim()
        let reg = /^[A-Za-z .]{2,50}/
        if(!reg.test(fname) ){
            setfNameErrMsg("Name should conatain atleast two and maximum 50 character")
            return false
            
        }else if (address?.length < 11){
            setAddrErrMsg('Address should be greater than 10 character')
            return false
        }
        else{
            setfNameErrMsg("")
            setAddrErrMsg("")
            if(nxt === "Next"){
                console.log("@@@@@", props.route.params.obj)
                const myObj = Object.assign({},props.route.params.obj,{firstName:fname, lastName: lname, address:address} )
                console.log("final---", myObj);
                props.navigation.navigate('Screen3', {obj:myObj})
            }
            return true
        }
    }
    return(
        <View style={styles.container}>
            <View style={{marginBottom:50 }}>
                <Text style={styles.txtLabel}>First Name</Text>
            <TextInput
            style={styles.txtInput}
            onChangeText={(e)=>nameValidate(key="fname",e)}
            maxLength={50}
            value={fname}
             /> 
             <Text style={styles.errMsg}>{fNameErrMsg}</Text>
             </View>

             <View style={{marginBottom:50 }}>
                <Text style={styles.txtLabel}>Last Name</Text>
            <TextInput
            style={styles.txtInput}
            onChangeText={(e)=>nameValidate(key="lname",e)}
            value={lname}
             /> 
             </View>

             <View style={{marginBottom:50 }}>
                <Text style={styles.txtLabel}>Address</Text>
            <TextInput
            style={styles.txtInput}
            onChangeText={(e)=>setAddress(e)}
             /> 
             <Text style={styles.errMsg}>{addrEMsg}</Text>
             </View>

             <View style={{flexDirection:'row'}}>
                <TouchableOpacity style={styles.btn} onPress={()=>props.navigation.goBack()}  >
                    <Text>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>formValidate()}>
                    <Text>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>formValidate("Next")}>
                    <Text>Save & Next</Text>
                </TouchableOpacity>
             </View>
              
        </View>
    )
}