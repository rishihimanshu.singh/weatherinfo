import React, { useState } from 'react';
import { View, Text,TextInput, StyleSheet, TouchableOpacity} from 'react-native'
import  SelectList from 'react-native-dropdown-select-list'
import styles from './Styles';

export default function Screen3(props){
    const [selected, setSelected] = useState(undefined);
    const [mobileNum, setMobileNum] = useState('');
    const [codeErrMsg,setCodeErrMsg] = useState('')
    const [mobErrMsg,setMobErrMsg] = useState('')
    const [toggleCheckBox, setToggleCheckBox] = useState(false)

    const data = [
        {key:'1', value:'91', disabled:true},
        {key:'2', value:'+1'},
    ]

    const frmValidate=()=>{
       if( mobileValidation(mobileNum) && countryCodeValidation(selected)){
            const myObj = Object.assign({},props.route.params.obj,{countryCode:selected, phoneNumber: mobileNum })
       }
    }

    const mobileValidation = (num) =>{
        const reg = /^[0-9]{10}$/;
        if(reg.test(num)){
           setMobErrMsg('')
           return true
        }
        else{
            setMobErrMsg("Please fill valid number")
            return false
        }
    }

    const countryCodeValidation = (code) =>{
        if(code === undefined){
            setCodeErrMsg('Please Select country code')
            return false
        }else{
            setCodeErrMsg('')
            return true
        }
    }
    return(
        <View style={styles.container3}>
            <View style={{ width:'80%'}}>
                <Text style={styles.txtLabel}>Country Code</Text>
                <SelectList  
                setSelected={(val) => setSelected(val)} 
                data={data} 
                save="value"
                /> 
                <Text>{codeErrMsg}</Text>
            </View>
            <View style={{width:'80%'}}>
                <Text style={styles.txtLabel}>Contact No.</Text>
                <TextInput keyboardType='number-pad' style={styles.txtInput} 
                onChangeText={(val)=>setMobileNum(val)}
                />
                <Text  style={styles.errMsg}>{mobErrMsg}</Text>
            </View>
            <View style={styles.innerContainer}>
                <TouchableOpacity style={[styles.chkBox,{backgroundColor: toggleCheckBox? 'transparent':'green'}]} onPress={()=>setToggleCheckBox(!toggleCheckBox)}>
                </TouchableOpacity>
                <Text style={styles.condtTxt}>Accept Terms And Condition</Text>
            </View>

            <View style={{flexDirection:'row', marginTop:20}}>
                <TouchableOpacity style={styles.btn} onPress={()=>props.navigation.goBack()} >
                    <Text>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>frmValidate()}>
                    <Text>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnDisable} disabled >
                    <Text>Save and Next</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}