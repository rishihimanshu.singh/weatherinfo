import React, { useState } from 'react';
import {Image, Text, TextInput, TouchableOpacity, View,StyleSheet} from 'react-native';
import axios from 'axios';

const Show = ()=>{

    const [srchTxt, setSrchTxt] = useState('')
    const [resData, setResData] = useState(undefined)
    const [errMsg, setErrMsg] = useState('')

    // API call
    const getAPIData = async(srch) =>{
        if(srch.length <1  ||srch > 10){
            setErrMsg("Please Fill the number between 1 to 10")
        }else{
            setErrMsg('')
            const getData = await axios.get('https://reqres.in/api/users/'+srch)  
            .then((res)=>{
            setResData(res?.data?.data)
            }).catch((error)=>{
            console.log("get eroor in response--", error);
            })
    }
}
  return(
    <View style={Styles.container}>
        <Text>User Data</Text>
        <View>
            <TextInput placeholder='Search' onChangeText={(e)=>setSrchTxt(e)} value={srchTxt} style={Styles.txtInput}/>
            <TouchableOpacity onPress={()=>getAPIData(srchTxt)} style={Styles.btn}>
                <Text>Search</Text>
            </TouchableOpacity>
            <Text style={Styles.errMsg}>{errMsg}</Text>
        </View>
        {resData !== undefined ?
         <View>
            <Text>User Id : {resData?.id}</Text>
            <Text>Email : {resData?.email}</Text>
            <Text>First Name : {resData?.first_name}</Text>
            <Text>Last Name : {resData?.last_name}</Text>
            <Image source={{uri: resData?.avatar}} style={Styles.img}/>
        </View> 
        : ''}
       
    </View>
  )
}
export default Show;

const Styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center', alignItems:'center'
    },
    txtInput:{
        border:1 ,borderWidth:1, borderColor:'black'
    },
    btn:{
        backgroundColor:'grey', padding:5, borderRadius:10
    },
    errMsg:{
        color:'red'
    },
    img:{
        height:100, width:100
    }

})