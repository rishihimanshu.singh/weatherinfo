import React from "react";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        margin:20
    },
    container3:{
        flex:1,alignItems:'center',justifyContent:'center',
    },
    txtLabel: {
        fontSize:18,
        color:'black',
        marginBottom:5
    },
    txtInput: {
        backgroundColor:'transparent', width:'100%', padding:10,borderWidth:1,borderColor:'black',borderRadius:10
    },
    btn:{
        backgroundColor:'blue',width:100,padding:10,justifyContent:'center',alignItems:'center',borderRadius:10,marginRight:5
    },
    errMsg:{
        color:'red'
    },
    btnDisable:{
        backgroundColor:'grey',width:150,padding:2,justifyContent:'center',alignItems:'center',borderRadius:10,marginRight:5   
    },
    innerContainer:{
        flexDirection:'row',justifyContent:'flex-start', alignSelf:'baseline',marginLeft:45,marginTop:15
    },
    chkBox:{
        height:15, width:15,borderWidth:1,marginRight:15
    },
    condtTxt:{
        paddingVertical:-5, fontSize:16
    }
    

})