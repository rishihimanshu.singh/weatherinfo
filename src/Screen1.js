import React, { useState } from 'react';
import { View, Text,TextInput, StyleSheet, TouchableOpacity} from 'react-native'
import styles from './Styles';

export default function Screen1(props){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState(undefined)

    const [emailErrMsg,setEmailErrMsg] = useState('')
    const [passErrMsg,setPassErrMsg] = useState('')
    
    const isValidEmail = Email => {

            let reg = /[A-Za-z 0-9._]{3,}@[A-Za-z]{3,}[.]{1}[A-Za-z]{2,6}/
            if(reg.test(Email)){
                setEmailErrMsg('')
                return true;
            }else{
                setEmailErrMsg("Please enter valid Email")
                return false
            }
       
      }

      const passwordValidate = pwd =>{
            let reg = /(?=(?:[^a-z]*[a-z]){2})(?=(?:[^A-Z]*[A-Z]){2})(?=(?:\D*\d){2})(?=.{8,})(?=(?:\w*\W){2})/
            console.log( "pwdddddd",reg.test(pwd))        
            if(reg.test(pwd)){
                setPassErrMsg('')
                return true;
            }else{
                setPassErrMsg("Password should have aleast two uppercase, two lowercase, two special character and two number")
                return false
            }
      }

    const formValidate= () =>{

        console.warn(password,email)
        if(isValidEmail(email) && passwordValidate(password) ){
            alert("Data successfully saved")
        //    props.navigation.navigate('Screen1')
        }
       
    }

    const nextScreen = () =>{
        if(isValidEmail(email) && passwordValidate(password) ){
            alert("Data successfully saved")
            const obj = {
                email: email,
                password: password
            }
            props.navigation.navigate('Screen2',{obj: obj})
        }
    }

    return(
        <View style={styles.container}>
            <View style={{marginBottom:50 }}>
                <Text style={styles.txtLabel}>Email Id</Text>
                <TextInput style={styles.txtInput} placeholder="Please Enter EmailId" onChangeText={(e)=>setEmail(e)} /> 
                <Text style={styles.errMsg}>{emailErrMsg}</Text>
             </View>

             <View style={{marginBottom:50 }}>
                <Text style={styles.txtLabel}>Password</Text>
                <TextInput style={styles.txtInput} placeholder="Password" onChangeText={(e)=>setPassword(e)} /> 
                <Text style={styles.errMsg}>{passErrMsg}</Text>
             </View>
             <View style={{flexDirection:'row'}}>
                <TouchableOpacity style={styles.btn}  disabled >
                    <Text>Back</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>formValidate()} >
                    <Text>Save</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btn} onPress={()=>nextScreen()} >
                    <Text>Save & Next</Text>
                </TouchableOpacity>
             </View>
              
        </View>
    )
}

