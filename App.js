import React from 'react';
import {View, Text} from 'react-native';
// import WeatherScreen from './src/WeatherScreen/WeatherScreen';
import Screen1 from './src/Screen1';
import Screen2 from './src/Screen2';
import Screen3 from './src/Screen3';
import Show from './src/Show';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const App =()=>{

    const Stack = createNativeStackNavigator()
  return(
   <NavigationContainer>
      <Stack.Navigator initialRouteName='Screen1'>
        <Stack.Screen name="Show" component={Show} />
        <Stack.Screen  name="Screen1" component={Screen1} />
        <Stack.Screen  name="Screen2"  component={Screen2}/>
        <Stack.Screen  name="Screen3" component={Screen3}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
export default App;
